package com.tsystems.javaschool.tasks.calculator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
	
    public String evaluate(String statement) {
        // TODO: Implement the logic here
    	//String expression validation check_start
    	if (statement == null)
    		return null;
    	if (statement.isEmpty() ||
    		statement.matches(".*[a-zA-Z].*") ||	//tried to use regex like [\\w]+ but it was hopeless
    		statement.startsWith("-(") ||
    		statement.contains("++") ||
            statement.contains("--") ||
            statement.contains("**") ||
            statement.contains("//") ||
            statement.contains("..") ||
            statement.contains("+-") ||
            statement.contains("-+") ||
            statement.contains("+*") ||
            statement.contains("*+") ||
            statement.contains("+/") ||
            statement.contains("/+") ||
            statement.contains("+.") ||
            statement.contains(".-") ||
            statement.contains("-*") ||
            statement.contains("*-") ||
            statement.contains("-/") ||
            statement.contains("/-") ||
            statement.contains("-.") ||
            statement.contains("-.") ||
            statement.contains("*/") ||
            statement.contains("/*") ||
            statement.contains("*.") ||
            statement.contains(".*") ||
            statement.contains("/.") ||
            statement.contains("./") ||
            statement.charAt(0) == '+' ||
         	statement.charAt(0) == '*' ||
			statement.charAt(0) == '/' ||
			statement.contains(","))
    		return null;
    	int validCheck = 0;
    	for (char c : statement.toCharArray()) {
    		if (c == '(') validCheck++;
    		else if (c == ')') validCheck--;
    		if (validCheck < 0) return null; //Chek that opening parentheses are always before closing
    	}
    	if (validCheck != 0) return null;		
    	//String expression validation check_end
    	
	     statement = statement.replaceAll(" ", "");
	     Matcher m = Pattern.compile("\\(([^)]+)\\)").matcher(statement);//found this regex somewhere in the stackoverflow
	     //while below should replace all the parentheses with evaluating of expression inside
	     while(m.find()) {
	    	 String temp = expSolve(m.group(1));
	    	 statement = statement.replace(m.group(1), temp).replaceAll("[(-)]+", "");//removing parentheses
	     }
	     //if below checks that previous operation did not put to mathematical symbols together
	     if (statement.contains("+-") || statement.contains("-+")) {
	    	 statement = statement.replace("+-", "-");
	    	 statement = statement.replace("-+", "-");
	     }

	     statement = expSolve(statement);//evaluating all the rest mathematical operations
	     if (statement == null) return null;
	     String temp = statement.substring(statement.indexOf(".") + 1);
	     if (temp.length() > 4)
	    	 statement = String.format("%.4f", Double.parseDouble(statement)).replace(",", ".");
	     if (Integer.parseInt(temp) == 0)
	    	 return String.format("%.0f", Double.parseDouble(statement)).replace(",", ".");
	     else
	         return statement;
    }

    //Don't know rules to name variables inside the method
	//and why I made static method (think, that it depends from how I'm using it) 
    //"plus" should evaluate mathematical operation plus
	private static String plus (String statement) {
    	double res = 0;
    	//Understood how to split string from
	    //https://stackoverflow.com/questions/7894686/how-to-get-to-a-particular-element-in-a-list-in-java
    	List<String> myList = Arrays.asList(statement.replaceAll("[^-0-9.,]+", " ").trim().split(" "));
    	for(String s : myList) {
    		if (statement.indexOf(s) < statement.indexOf("+") &&
    			statement.indexOf(s) + s.length() == statement.indexOf("+"))
    			res = Double.parseDouble(s) + Double.parseDouble(myList.get(myList.indexOf(s) + 1));
	}
	return Double.toString(res);
    }
	
    private static String minus (String statement) {
    	double res = 0;
    	List<String> myList = Arrays.asList(statement.replaceAll("[^0-9.,]+", " ").trim().split(" "));
    	for(String s : myList) {
    		if (statement.charAt(0) == '-') {
    		  res = Double.parseDouble(myList.get(myList.indexOf(s) + 1)) - Double.parseDouble(s);
    		  break;
    		}
    		else if (statement.indexOf(s) < statement.indexOf("-") &&
    			     statement.indexOf(s) + s.length() == statement.indexOf("-")) {        			
    			res = Double.parseDouble(s) - Double.parseDouble(myList.get(myList.indexOf(s) + 1));
    			break;
    		}
	}
	return Double.toString(res);
    }
    
    private static String multiply (String statement) {
    	double res = 0;
    	List<String> myList = Arrays.asList(statement.replaceAll("[^0-9.,]+", " ").trim().split(" "));
    	for(String s : myList) {
    		if (statement.indexOf(s) < statement.indexOf("*") &&
    			statement.indexOf(s) + s.length() == statement.indexOf("*"))
    			res = Double.parseDouble(s) * Double.parseDouble(myList.get(myList.indexOf(s) + 1));        		
    	}
    	if (statement.charAt(0) == '-') res = res * (-1);
	return Double.toString(res);
    }
    
    private static String divide (String statement) {
    	double res = 0;
    	List<String> myList = Arrays.asList(statement.replaceAll("[^-0-9.,]+", " ").trim().split(" "));
    	for(String s : myList) {
    		if (statement.indexOf(s) < statement.indexOf("/") &&
    			statement.indexOf(s) + s.length() == statement.indexOf("/"))
     			res = Double.parseDouble(s) / Double.parseDouble(myList.get(myList.indexOf(s) + 1));
    	}
    	return Double.toString(res);
    }
    
    //Method below should choose wich mathematical operation to evaluate
    private static String selection (String statement) {
    	if (statement.contains("*"))
    		return multiply(statement);
    	else if (statement.contains("/"))
    		return divide(statement);
    	else if (statement.charAt(0) == '-')//I don't remember what for have I done this condition
    		return minus(statement);
    	else if (statement.contains("+"))
    		return plus(statement);
    	else if (statement.contains("-"))
    		return minus(statement);
    	else return "Something wrong!";
    }
    
    //Method below chooses which part of expression going to be evaluated 
    private static String subStatement (String statement) {
    	String res = null;
    	int controlIndex = 0;
    	List<String> controlList = new ArrayList<String>();
    	List<String> myList = new ArrayList<String>();
   		controlList = Arrays.asList(statement.replaceAll("[0-9.,]+", " ").trim().split(" "));
		myList = Arrays.asList(statement.replaceAll("[^0-9.,]+", " ").trim().split(" "));
    	for (String s : myList) {        		
    		if (statement.indexOf(s) < statement.indexOf("*") &&
    			controlIndex == controlList.indexOf("*")) {
    			res = s + "*" + myList.get(controlIndex + 1);
    			return res;
    		}        			
    		else if (statement.indexOf(s) < statement.indexOf("/") &&
    				 controlIndex == controlList.indexOf("/")) {
    			if(Double.parseDouble(myList.get(controlIndex + 1)) != 0)
    					res = s + "/" + myList.get(controlIndex + 1);
    			else return null;
    			return res;
    		}
    		else if (statement.indexOf(s) < statement.indexOf("+") &&
    				controlIndex == controlList.indexOf("+") &&
    				!statement.contains("*") && !statement.contains("/")) {
    			res = s + "+" + myList.get(controlIndex + 1);
    			return res;
    		}
    		else if (statement.indexOf(s) < statement.indexOf("-") &&
    				controlIndex == controlList.indexOf("-") &&
      				 !statement.contains("*") && !statement.contains("/")) {
      			res = s + "-" + myList.get(controlIndex + 1);
      			return res;
       		}
    		else if (statement.indexOf(s) < statement.indexOf("/-") &&//condition to evaluate dividing t0 negative number
    				controlIndex == controlList.indexOf("/-")) {
    			if(Double.parseDouble(myList.get(controlIndex + 1)) != 0)
    				res = s + "/" + "-" + myList.get(controlIndex + 1);
    			else return null;
      			return res;
       		}
    		else if (statement.indexOf(s) < statement.indexOf("*-") &&
    				controlIndex == controlList.indexOf("*-")) {
      			res = s + "*" + "-" + myList.get(controlIndex + 1);
      			return res;
       		}
        	controlIndex++;
    	}
    	return statement;//Not very clever, but it's working (hoping that with T-Systems I'll become skillful programmer
    }
    
    //Method replacing part of expression with this part evaluation result
    private static String expSolve (String exp) {
	     while (exp.contains("*") || 
			    	exp.contains("/") || 
			    	exp.contains("+") ||
			    	exp.contains("-")) {
			    	 if (!exp.contains("*") &&
			    		 !exp.contains("/") &&
			 	    	 !exp.contains("+") &&
			 	    	  exp.lastIndexOf("-") == 0) break;//we need it if we got negative number
			    	 if(subStatement(exp) == null) return null;
			    	 exp = exp.replace(subStatement(exp), selection(subStatement(exp)));
		     }
	     return exp;
    }
 
}

